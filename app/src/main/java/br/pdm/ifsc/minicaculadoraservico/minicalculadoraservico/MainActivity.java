package br.pdm.ifsc.minicaculadoraservico.minicalculadoraservico;

import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.SeekBar;
import android.widget.TextView;
import java.text.*;



public class MainActivity extends AppCompatActivity {
    // objetos para formatar os números na interface
    private static NumberFormat currencyFormat = NumberFormat.getCurrencyInstance();
    private static NumberFormat percentFormat = NumberFormat.getPercentInstance();
    // variáveis da lógica da aplicação
    private double billAmount = 0.0; // valor da conta inserido pelo usuário
    private double percent = 0.15; // valor da taxa de serviço – 15% por padrão
    // variáveis para referenciar os componentes gráficos da tela
    private TextView amountTextView;
    private TextView percentTextView;
    private TextView tipTextView;
    private TextView totalTextView;

    // variáveis para detectar o valor da aceleração
    private float acceleration;
    private float currentAcceleration;
    private float lastAcceleration;

    // constante para detectar se o usuário chacoalhou o dispositivo
    private static final int ACCELERATION_THRESHOLD = 100000;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // Inicialização padrão de uma atividade
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        // Criando as refências dos componentes gráficos da tela
        amountTextView = (TextView) findViewById(R.id.amountTextView);
        percentTextView = (TextView) findViewById(R.id.percentTextView);
        tipTextView = (TextView) findViewById(R.id.tipTextView);
        totalTextView = (TextView) findViewById(R.id.totalTextView);
        // coloca como texto inicial das Views tip e total como 0
        tipTextView.setText(currencyFormat.format(0));
        totalTextView.setText(currencyFormat.format(0));
        // Acessa o campo de texto (EditText) da tela e configura
        // o tratamento de eventos para mudanças (Change Listener)
        EditText amountEditText = (EditText) findViewById(R.id.amountEditText);
        amountEditText.addTextChangedListener(amountEditTextWatcher);
        // Acessa a seeker bar da tela e configura o tratamento
        // de eventos para mudanças na barra (Change Listener)
        SeekBar percentSeekBar = (SeekBar) findViewById(R.id.percentSeekBar);
        percentSeekBar.setOnSeekBarChangeListener(seekBarListener);
        // inicialização dos valores de aceleração

    }



    private void calculate() {
        // formata e mostra a percentagem correta na percentTextView da tela
        percentTextView.setText(percentFormat.format(percent));
        // calcula o valor da gorjeta e o novo valor total
        double tip = billAmount * percent;
        double total = billAmount + tip;
        // formata e mostra os novos valores do troco e total na tela
        tipTextView.setText(currencyFormat.format(tip));
        totalTextView.setText(currencyFormat.format((total)));
    }

    private final SeekBar.OnSeekBarChangeListener seekBarListener = new
            SeekBar.OnSeekBarChangeListener() {
                @Override
                public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
                    // atualiza o novo valor da porcentagem
                    // a variável "i" representa o valor na barra de progesso
                    percent = i / 100.0;
                    // invoca o método calculate para atualizar a nova percentagem
                    // (re) calcular o valor do troco e total
                    calculate();
                }

                @Override
                public void onStartTrackingTouch(SeekBar seekBar) {
                }

                @Override
                public void onStopTrackingTouch(SeekBar seekBar) {
                }
            };

    private final TextWatcher amountEditTextWatcher = new TextWatcher() {
        @Override
        public void onTextChanged(CharSequence charSequence,
                                  int i, int i1, int i2) {
            // tratamento de exceção para prevenção de erros
            try {
                // obtem o valor da conta e mostra em valor de moeda na
                // amountTextView
                billAmount = Double.parseDouble(charSequence.toString()) / 100.0;
                amountTextView.setText(currencyFormat.format(billAmount));
            } catch (NumberFormatException e) {
                // se a entrada for um valor em branco ou não numérico, o cálculo
                // dará errado e disparará a exceção NumberFormatException
                amountTextView.setText(R.string.enter_amount);
                billAmount = 0.0;
            }
            // invoca o método calculate para atualizar a nova percentagem
            // (re) calcular o valor do troco e total
            calculate();
        }

        @Override
        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) { }

        @Override
        public void afterTextChanged(Editable editable) {}
    };

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // obtem acesso ao objeto MenuInflater
        MenuInflater inflater = getMenuInflater();
        // adiciona o layout menu.xml ao menu da atividade
        inflater.inflate(R.menu.menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // descobri qual opção foi pressionada
        switch (item.getItemId()){
            case R.id.delete_menu:
                delete();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public void delete() {
        tipTextView.setText(currencyFormat.format(0)); // R$ 0,00 ou U$0,00
        totalTextView.setText(currencyFormat.format(0)); // R$ 0,00 ou U$0,00
        EditText amountEditText = (EditText) findViewById(R.id.amountEditText);
        amountEditText.setText("0");
        amountTextView.setText(R.string.enter_amount);
        billAmount = 0.0;
    }
}
